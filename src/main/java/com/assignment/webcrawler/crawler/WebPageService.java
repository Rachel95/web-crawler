package com.assignment.webcrawler.crawler;

import db.model.tables.records.WebPageRecord;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static db.model.Tables.WEB_PAGE;

@Slf4j
@Service
public class WebPageService {

    private DSLContext dslContext;

    public WebPageService(final DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    public void create(final String uri,
                       final String content,
                       final String metaTags,
                       final String title,
                       final String canonicalUri) {
        WebPageRecord record = dslContext.newRecord(WEB_PAGE);
        record.setId(UUID.randomUUID());
        record.setUri(uri);
        record.setContent(content);
        record.setTitle(title);
        record.setCanonicalUri(canonicalUri);
        record.setMetaTags(metaTags);
        try {
            record.store();
        } catch (Exception e) {
            log.error("Could not insert record into database: " + e.getMessage());
        }
    }

    public boolean exists(final String uri) {
        return dslContext.fetchExists(dslContext.selectOne()
                .from(WEB_PAGE)
                .where(WEB_PAGE.URI.equal(uri)));
    }

    public void createNew(final String uri,
                          final String content,
                          final String metaTags,
                          final String title,
                          final String canonicalUri) {
        dslContext.insertInto(WEB_PAGE)
                .set(WEB_PAGE.URI, uri)
                .set(WEB_PAGE.CONTENT, content)
                .set(WEB_PAGE.META_TAGS, metaTags)
                .set(WEB_PAGE.TITLE, title)
                .set(WEB_PAGE.CANONICAL_URI, canonicalUri)
                .set(WEB_PAGE.ID, UUID.randomUUID())
                .onDuplicateKeyUpdate()
                .set(WEB_PAGE.CONTENT, content)
                .set(WEB_PAGE.META_TAGS, metaTags)
                .set(WEB_PAGE.TITLE, title)
                .set(WEB_PAGE.CANONICAL_URI, canonicalUri)
                .execute();
    }
}
