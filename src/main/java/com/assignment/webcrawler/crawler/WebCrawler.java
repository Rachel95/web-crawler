package com.assignment.webcrawler.crawler;

import com.panforge.robotstxt.RobotsTxt;
import com.panforge.robotstxt.client.HttpClientWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.net.URI;
import java.util.*;


@Slf4j
@Component
public class WebCrawler {

    private static final String ROBOTS_TXT_FILE = "robots.txt";
    private static final int MAX_DEPTH = 3;

    private static final HashSet<String> links = new HashSet<>();
    private static final HashSet<String> seeds = new HashSet<>();

    static {
        seeds.add("http://www.allfreethings.com");
        seeds.add("http://www.freeprwebdirectory.com");
        //seeds.add("http://www.traveltourismdirectory.info");
        seeds.add("http://www.highrankdirectory.com");
        seeds.add("http://www.britainbusinessdirectory.com");
        seeds.add("http://www.marketinginternetdirectory.com");
        seeds.add("http://www.ukinternetdirectory.com");
        seeds.add("http://uk.yahoo.com");
        seeds.add("http://www.dmoz-odp.org/");
    }

    private final WebPageService service;

    public WebCrawler(final WebPageService service) {
        this.service = service;
    }

    //scan seeds every hour
    @Scheduled(fixedDelay = 3600000, initialDelay = 1000)
    public void process() {
        seeds.forEach(url -> processLink(url, checkForRobotsTxtFile(url), 0));
    }

    public void processLink(final String url, final Optional<RobotsTxt> robotsTxt, final int depth) {
        if (robotsTxt.isPresent()) {
            if (robotsTxt.get().query(null, url)) {
                getPageLinks(url, depth);
            }
        } else {
            getPageLinks(url, depth);
        }
    }

    public void getPageLinks(final String url, int depth) {
        if (!links.contains(url) && (depth < MAX_DEPTH)) {
            if (links.add(url)) {
                log.info("Processing URL: " + url);
            }

            try {
                log.info("Connecting to: " + url);

                Document document = Jsoup
                        .connect(url)
                        .userAgent(RandomUserAgent.getRandomUserAgent())
                        .timeout(2000)
                        .get();

                Elements linksOnPage = document.select("a[href]");
                log.info("Got links: " + linksOnPage.size());

                String canonicalUrl = getCanonicalUrl(document);
                List<String> titles = getTitles(document);
                Map<String, String> metaTags = getMetaTags(document);

                log.info("Canonical URL: " + canonicalUrl);
                log.info("Titles: " + titles);
                log.info("Meta: " + metaTags);

                //Check meta tags to see if we can index this page
                if(canIndex(metaTags)) {
                    log.info("Indexing page: " + url);
                    service.create(url,
                            document.toString(),
                            getMetaTags(document).toString(),
                            String.valueOf(getTitles(document)),
                            getCanonicalUrl(document));
                } else {
                    log.info("NOINDEX on: " + url);
                }

                depth++;
                for (Element page : linksOnPage) {
                    String nextUrl = page.attr("abs:href");
                    if(!StringUtils.isEmpty(nextUrl) || !StringUtils.isBlank(nextUrl)) {
                        getPageLinks(nextUrl, depth);
                    }
                }
            } catch (Exception e) {
                System.err.println("Error Processing '" + url + "': " + e.getMessage());
            }
        }
    }

    /**
     * Checks for presence of Robots.txt file on URL
     *
     * @param url The URL to query for robot file
     * @return Optional RobotsTxt
     */
    private Optional<RobotsTxt> checkForRobotsTxtFile(final String url) {
        RobotsTxt robotsTxt = null;
        try (CloseableHttpClient httpClient = new HttpClientWrapper(HttpClients.createSystem())) {
            URI uri = new URI(url);
            HttpGet httpGet = new HttpGet(uri.getHost() + "/" + ROBOTS_TXT_FILE);
            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                final HttpEntity entity = response.getEntity();
                if (entity != null) {
                    try (InputStream inputStream = entity.getContent()) {
                        robotsTxt = RobotsTxt.read(inputStream);
                        return Optional.of(robotsTxt);
                    }
                } else {
                    log.info("No Robots.txt file");
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return Optional.empty();
    }

    /**
     * Get Map of Meta Tags
     *
     * @param doc JSOUP Document
     * @return Map of Meta Tags
     */
    private Map<String, String> getMetaTags(final Document doc) {
        Elements metaTags = doc.getElementsByTag("meta");

        Map<String, String> metaInformation = new HashMap<>();

        for (Element metaTag : metaTags) {
            String property = metaTag.attr("property");
            String content = metaTag.attr("content");
            metaInformation.put(property, content);
        }

        return metaInformation;
    }

    /**
     * Get Canonical URL from Document
     *
     * @param doc JSOUP Document
     * @return canonical URL
     */
    private String getCanonicalUrl(final Document doc) {
        Element cannonicalUrl = doc.select("link[rel=canonical]").first();
        return cannonicalUrl != null ? cannonicalUrl.text() : "";
    }

    /**
     * Get titles from document
     *
     * @param doc JSOUP Document
     * @return List of Titles.
     */
    private List<String> getTitles(final Document doc) {
        Elements titles = doc.getElementsByTag("title");

        List<String> extractedTitles = new ArrayList<>();

        for (Element title : titles) {
            if (title != null) {
                extractedTitles.add(title.text());
            }
        }

        return extractedTitles;
    }

    private boolean canIndex(Map<String, String> metaTags) {
        for (Map.Entry<String, String> entry : metaTags.entrySet()) {
            if(entry.getKey().contains("NOINDEX") || entry.getValue().contains("NOINDEX")) {
                return false;
            }
            if(entry.getKey().contains("NOFOLLOW") || entry.getValue().contains("NOFOLLOW")) {
                return false;
            }
        }
        return true;
    }
}
