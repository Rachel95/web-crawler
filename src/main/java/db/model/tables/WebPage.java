/*
 * This file is generated by jOOQ.
 */
package db.model.tables;


import db.model.Indexes;
import db.model.Keys;
import db.model.Public;
import db.model.tables.records.WebPageRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row7;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class WebPage extends TableImpl<WebPageRecord> {

    private static final long serialVersionUID = 1116458688;

    /**
     * The reference instance of <code>public.web_page</code>
     */
    public static final WebPage WEB_PAGE = new WebPage();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<WebPageRecord> getRecordType() {
        return WebPageRecord.class;
    }

    /**
     * The column <code>public.web_page.id</code>.
     */
    public final TableField<WebPageRecord, UUID> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>public.web_page.uri</code>.
     */
    public final TableField<WebPageRecord, String> URI = createField(DSL.name("uri"), org.jooq.impl.SQLDataType.VARCHAR(2000).nullable(false), this, "");

    /**
     * The column <code>public.web_page.indexed</code>.
     */
    public final TableField<WebPageRecord, Timestamp> INDEXED = createField(DSL.name("indexed"), org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.field("now()", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>public.web_page.content</code>.
     */
    public final TableField<WebPageRecord, String> CONTENT = createField(DSL.name("content"), org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>public.web_page.meta_tags</code>.
     */
    public final TableField<WebPageRecord, String> META_TAGS = createField(DSL.name("meta_tags"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.web_page.title</code>.
     */
    public final TableField<WebPageRecord, String> TITLE = createField(DSL.name("title"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.web_page.canonical_uri</code>.
     */
    public final TableField<WebPageRecord, String> CANONICAL_URI = createField(DSL.name("canonical_uri"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * Create a <code>public.web_page</code> table reference
     */
    public WebPage() {
        this(DSL.name("web_page"), null);
    }

    /**
     * Create an aliased <code>public.web_page</code> table reference
     */
    public WebPage(String alias) {
        this(DSL.name(alias), WEB_PAGE);
    }

    /**
     * Create an aliased <code>public.web_page</code> table reference
     */
    public WebPage(Name alias) {
        this(alias, WEB_PAGE);
    }

    private WebPage(Name alias, Table<WebPageRecord> aliased) {
        this(alias, aliased, null);
    }

    private WebPage(Name alias, Table<WebPageRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> WebPage(Table<O> child, ForeignKey<O, WebPageRecord> key) {
        super(child, key, WEB_PAGE);
    }

    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.WEB_PAGE_PKEY, Indexes.WEB_PAGE_SEARCH_IDX, Indexes.WEB_PAGE_URI_KEY);
    }

    @Override
    public UniqueKey<WebPageRecord> getPrimaryKey() {
        return Keys.WEB_PAGE_PKEY;
    }

    @Override
    public List<UniqueKey<WebPageRecord>> getKeys() {
        return Arrays.<UniqueKey<WebPageRecord>>asList(Keys.WEB_PAGE_PKEY, Keys.WEB_PAGE_URI_KEY);
    }

    @Override
    public WebPage as(String alias) {
        return new WebPage(DSL.name(alias), this);
    }

    @Override
    public WebPage as(Name alias) {
        return new WebPage(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public WebPage rename(String name) {
        return new WebPage(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public WebPage rename(Name name) {
        return new WebPage(name, null);
    }

    // -------------------------------------------------------------------------
    // Row7 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row7<UUID, String, Timestamp, String, String, String, String> fieldsRow() {
        return (Row7) super.fieldsRow();
    }
}
